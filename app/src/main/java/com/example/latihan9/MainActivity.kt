package com.example.latihan9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlin.math.floor

class MainActivity : AppCompatActivity() {
    private var nomorAntrian = 0
    lateinit var tampilanNomer: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tampilanNomer = findViewById<TextView>(R.id.tv_angka)
    }

    fun fTambah(view: android.view.View) {
        nomorAntrian += if (Math.random() > 0.9) {
            3
        } else {
            40
        }

        when (floor((nomorAntrian/10).toDouble()).toInt()) {
            1 -> Roti("diatas 10")
            2 -> Roti("diatas 20")
            3 -> Roti("diatas 30")
            4 -> Roti("diatas 40")
            5 -> Roti("diatas 50")
            6 -> Roti("diatas 60")
            7 -> Roti("diatas 70")
            8 -> Roti("diatas 80")
            9 -> Roti("diatas 90")
            else -> Roti("diatas 100")
        }

        tampilanNomer.text = nomorAntrian.toString()
    }

    fun fReset(view: android.view.View) {
        Roti( "Antrian di reset", true)
        nomorAntrian = 0
        tampilanNomer.text = nomorAntrian.toString()
    }

    fun Roti(text: String, panjang: Boolean = false) {
        Toast.makeText(applicationContext, text, if(panjang) Toast.LENGTH_LONG else Toast.LENGTH_SHORT).show()
    }
}